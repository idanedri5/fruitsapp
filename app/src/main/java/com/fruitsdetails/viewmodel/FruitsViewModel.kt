package com.fruitsdetails.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.fruitsdetails.model.entities.FruitDetails
import com.fruitsdetails.model.network.FruitsRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.observers.DisposableSingleObserver
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

@HiltViewModel
class FruitsViewModel @Inject constructor(private val repository: FruitsRepository) : ViewModel() {
    private var FruitsliveDataList: MutableLiveData<List<FruitDetails>> = MutableLiveData()


    fun getLiveDataObserver(): MutableLiveData<List<FruitDetails>>{
        return FruitsliveDataList
    }

    fun loadListOfData() {
        repository.makeFruitsApiCall(FruitsliveDataList)
    }
}