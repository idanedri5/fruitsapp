package com.fruitsdetails.utils

import com.fruitsdetails.model.network.FruitsServiceInstance
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)

object Constants {
    private const val BaseUrl: String = "https://dev-api.com/fruitsBT/"
    const val END_POINT: String = "getFruits"

    @Singleton
    @Provides
    fun getFruitsServiceInstance(retrofit: Retrofit): FruitsServiceInstance {
        return retrofit.create(FruitsServiceInstance::class.java)
    }

    @Singleton
    @Provides
    fun getAllFruits(): Retrofit {

        return Retrofit.Builder()
            .baseUrl(BaseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

}