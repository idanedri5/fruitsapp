package com.fruitsdetails.model.network

import com.fruitsdetails.model.entities.FruitDetails
import com.fruitsdetails.model.entities.FruitsApiData
import com.fruitsdetails.utils.Constants
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query


interface FruitsServiceInstance {

    @GET(Constants.END_POINT)
     fun getFruitsFromApi(): Call<FruitsApiData>

}