package com.fruitsdetails.view.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.manager.SupportRequestManagerFragment
import com.fruitsdetails.R
import com.fruitsdetails.databinding.ActivityFruitsBinding
import com.fruitsdetails.databinding.FruitsRowBinding
import com.fruitsdetails.view.adapters.FruitsRvAdapter
import com.fruitsdetails.viewmodel.FruitsViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_fruit_details.*

@AndroidEntryPoint
class FruitsActivity : AppCompatActivity() {

    private lateinit var fruitsBinding: ActivityFruitsBinding
    private lateinit var fruitsRvAdapter: FruitsRvAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fruits)



        fruitsBinding = ActivityFruitsBinding.inflate(layoutInflater)
        setContentView(fruitsBinding.root)
        var fruitsRowBinding = FruitsRowBinding.inflate(layoutInflater)


        initFruitsRecycleView()
        initViewModel()


    }


    private fun initFruitsRecycleView(){

        fruitsRvAdapter = FruitsRvAdapter(this@FruitsActivity)
        fruitsBinding.fruitsRecyclerView.layoutManager = LinearLayoutManager(this)
        fruitsBinding.fruitsRecyclerView.adapter = fruitsRvAdapter

    }

    private fun initViewModel(){


      val viewModel: FruitsViewModel =  ViewModelProvider(this).get(FruitsViewModel::class.java)

      viewModel.getLiveDataObserver().observe(this  ,  Observer{
          if (it != null) {
              fruitsRvAdapter.setListData(it)
              fruitsRvAdapter.notifyDataSetChanged()
          }else{
              Toast.makeText(this, "error to get data from server", Toast.LENGTH_SHORT).show()
          }

      })
        viewModel.loadListOfData()
    }


}