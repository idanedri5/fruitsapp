package com.fruitsdetails.view.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.fruitsdetails.R
import com.fruitsdetails.databinding.ActivityFruitDetailsBinding
import com.fruitsdetails.databinding.ActivityFruitsBinding
import hilt_aggregated_deps._com_fruitsdetails_viewmodel_FruitsViewModel_HiltModules_BindsModule
import kotlinx.android.synthetic.main.activity_fruits.*
import kotlinx.android.synthetic.main.fruits_row.view.*

class FruitDetailsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fruit_details)
        supportActionBar?.let {
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
        }


       val fruitDetailsBinding =  ActivityFruitDetailsBinding.inflate(layoutInflater)
        setContentView(fruitDetailsBinding.root)

        fruitDetailsBinding.fruitDetailsName.text = intent.getStringExtra("name")
        fruitDetailsBinding.fruitDetailsDescription.text = intent.getStringExtra("description")
        fruitDetailsBinding.fruitDetailsPrice.text = intent.getStringExtra("price")
        val imageUri = intent.getStringExtra("image")

        val fruitImage: ImageView = fruitDetailsBinding.fruitDetailsImage
        Glide.with(fruitImage).load(imageUri).into(fruitImage)



    }
}