package com.fruitsdetails.view.adapters

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.fruitsdetails.R
import com.fruitsdetails.databinding.ActivityFruitsBinding
import com.fruitsdetails.databinding.FruitsRowBinding
import com.fruitsdetails.model.entities.FruitDetails
import com.fruitsdetails.utils.Extentions.Int.toPrice
import com.fruitsdetails.utils.Extentions.Int.toPrice
import com.fruitsdetails.view.activity.FruitDetailsActivity
import com.fruitsdetails.view.activity.FruitsActivity
import kotlinx.android.synthetic.main.fruits_row.view.*
import kotlinx.coroutines.GlobalScope

class FruitsRvAdapter(private val activity: Activity)
    : RecyclerView.Adapter<FruitsRvAdapter.ViewHolder>() {


    private var fruitsListData: List<FruitDetails>? = null
    lateinit var parent: ViewGroup


    fun setListData(listData: List<FruitDetails>?) {
        this.fruitsListData = listData
    }


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int,
    ): FruitsRvAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.fruits_row, parent, false)
        this.parent = parent

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: FruitsRvAdapter.ViewHolder, position: Int) {
        fruitsListData?.let {
            holder.bind(fruitsListData!![position])



            holder.itemView.setOnClickListener {
                if (activity is FruitsActivity) {
                    val fruitsRowBinding: ActivityFruitsBinding =
                        ActivityFruitsBinding.inflate(activity.layoutInflater, parent, false)
                    val intent: Intent = Intent(activity, FruitDetailsActivity::class.java).apply {
                        putExtra("name", fruitsListData!![position].name)
                        putExtra("image", fruitsListData!![position].image)
                        putExtra("description", fruitsListData!![position].description)
                        putExtra("price", fruitsListData!![position].price?.toPrice())

                    }

                   parent.context.startActivity(intent)


                    /*fruitsRowBinding.fruitsRecyclerRoot.bindVisibility(false)
                    fruitsRowBinding.fruitsRecyclerView.bindVisibility(false)
                    activity.supportFragmentManager.beginTransaction()
                        .replace(R.id.fruits_recycler_root, FruitDetailsFragment())
                        .addToBackStack("replacement").commit()*/

                }
            }

        }
    }


    override fun getItemCount(): Int {

        if (fruitsListData == null || fruitsListData?.size == 0) {
            return 1
        } else {
            fruitsListData?.let {
                return fruitsListData!!.size
            }
        }
        return 0
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val fruitName: TextView = view.fruit_name
        private val fruitImage: ImageView = view.fruit_image

        fun bind(data: FruitDetails) {
            fruitName.text = data.name
            Glide.with(fruitImage)
                .load(data.image)
                .into(fruitImage)
        }

    }
}
